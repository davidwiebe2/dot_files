call plug#begin()
    Plug 'edkolev/tmuxline.vim'
    Plug 'alampros/vim-styled-jsx'
    Plug 'rust-lang/rust.vim'
    Plug 'mustache/vim-mustache-handlebars'
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
    Plug 'junegunn/fzf.vim'
    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-unimpaired'
    "Plug 'tpope/vim-sleuth'
    Plug 'tpope/vim-db'
    Plug 'mhinz/vim-grepper'
    Plug 'Chiel92/vim-autoformat'
    Plug 'pacha/vem-tabline'
    Plug 'itchyny/lightline.vim'
    Plug 'majutsushi/tagbar'
    "Plug 'xolox/vim-easytags'
    Plug 'universal-ctags/ctags'
    Plug 'xolox/vim-misc'
    Plug 'scrooloose/nerdtree'
    Plug 'Xuyuanp/nerdtree-git-plugin'
    "Plug 'ryanoasis/vim-devicons' need a compatible font for this
    "Plug 'scrooloose/syntastic'
    Plug 'w0rp/ale'
    Plug 'benekastah/neomake'
    Plug 'editorconfig/editorconfig-vim'
    Plug 'flazz/vim-colorschemes'
    Plug 'bluz71/vim-moonfly-colors'
    Plug 'mhinz/vim-signify'
    Plug 'Shougo/deoplete.nvim'
    Plug 'ternjs/tern_for_vim', { 'do': 'npm install' }
    Plug 'severin-lemaignan/vim-minimap'
    Plug 'joonty/vdebug'
    Plug 'simnalamburt/vim-mundo'
    Plug 'ashisha/image.vim'
    Plug 'qpkorr/vim-bufkill'
    Plug 'machakann/vim-highlightedyank'
    Plug 'SirVer/ultisnips'
    Plug 'honza/vim-snippets'
    Plug 'fenetikm/falcon'
    Plug 'tpope/vim-markdown'
    Plug 'reedes/vim-colors-pencil'
    Plug 'rakr/vim-one'
    Plug 'burnettk/vim-angular'
    Plug 'othree/html5.vim'
    Plug 'pangloss/vim-javascript'
    Plug 'othree/javascript-libraries-syntax.vim'
    Plug 'matthewsimo/angular-vim-snippets'
    Plug 'claco/jasmine.vim'
    Plug 'liuchengxu/vim-which-key'
call plug#end()

filetype plugin indent on
let mapleader = ","
set clipboard+=unnamedplus

set softtabstop=4
set shiftwidth=4
set tabstop=4
set expandtab

autocmd Filetype javascript setlocal ts=2 sts=2 sw=2
autocmd Filetype json setlocal ts=2 sts=2 sw=2
autocmd Filetype yml setlocal ts=2 sts=2 sw=2
autocmd Filetype yaml setlocal ts=2 sts=2 sw=2

if &diff
    " diff mode
    set diffopt+=iwhite
endif

set hlsearch
set number
set relativenumber
set backspace=indent,eol,start
set hidden
syntax on

set termguicolors
"colorscheme one
"colorscheme falcon
"colorscheme pencil
"let g:pencil_higher_contrast_ui=1
"let g:pencil_gutter_color=1
"let g:pencil_neutral_code_bg=1

colorscheme falcon
"colorscheme moonfly
"colorscheme colorsbox-material
"colorscheme iceberg
"colorscheme wombat256i
"colorscheme railscasts
"colorscheme gruvbox
"colorscheme gotham256
"colorscheme Revolution
"colorscheme jellybeans
"colorscheme OceanicNext
"colorscheme nightsky
"colorscheme solarized
"colorscheme delek
"colorscheme mike_wombat

set background=dark

set mouse=a

set foldmethod=syntax
set foldlevel=20
set foldlevelstart=20

"let g:syntastic_ignore_files = ['^/usr/', '*node_modules*', '*vendor*', '*build*', '*LOCAL*', '*BASE', '*REMOTE*']
"let g:syntastic_mode_map = { 'mode': 'active' }
""let g:syntastic_javascript_checkers=['jshint', 'jscs']
"let g:syntastic_javascript_checkers=['jshint']
"let g:syntastic_php_phpcs_args = "--standard=.git/hooks/phpcs-ruleset.xml"
"let g:syntastic_php_phpmd_post_args = ".git/hooks/phpmd-ruleset.xml"
let php_sql_query = 1
let php_htmlInStrings = 1
let g:neomake_javascript_enabled_makers = ['jshint', 'jscs']
autocmd! BufWritePost * Neomake

set laststatus=2
let g:vem_tabline_show=2
let g:vem_tabline_multiwindow_mode=0
let g:lightline = {
\  'enable': {'tabline': 0},
\  'active': {
\    'left': [
\      ['mode', 'paste'],
\      ['gitbranch', 'readonly', 'filename', 'modified']
\    ]
\  },
\  'component_function': {
\    'gitbranch': 'fugitive#head'
\  },
\}

let g:tmuxline_preset = 'full'

"let g:ctrlp_max_files=0
"let g:ctrlp_custom_ignore = 'node_modules\|git\|vendor'

" Use deoplete.
let g:deoplete#enable_at_startup = 1

let ycm_autoclose_preview_window_after_completion=1

cnoreabbrev <expr> W ((getcmdtype() is# ':' && getcmdline() is# 'W')?('w'):('W'))
cnoreabbrev <expr> Q ((getcmdtype() is# ':' && getcmdline() is# 'Q')?('q'):('q'))

nnoremap <cr> :noh<CR><CR>:<backspace>
autocmd StdinReadPre * let s:std_in=1

nmap <leader>H <Plug>vem_move_buffer_left-
nmap <leader>L <Plug>vem_move_buffer_right-
nmap <leader>h <Plug>vem_prev_buffer-
nmap <leader>l <Plug>vem_next_buffer-

let g:vdebug_options = {
    \ 'server': '0.0.0.0'
\}

map gh :wincmd h<CR>
map gj :wincmd j<CR>
map gk :wincmd k<CR>
map gl :wincmd l<CR>

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
map <leader>n :NERDTreeToggle<CR><CR>:<backspace>
map <leader>N :NERDTreeFind<CR><CR>:<backspace>
map <leader>u :MundoToggle<CR><CR>:<backspace>

nnoremap <leader>s  :Grepper -tool git<cr>
nnoremap <leader>t  :TagbarToggle<cr>

map <leader>f :FZF<CR>
map <leader>F :Buffers<CR>

set tags=./.tags;,~/.vimtags
"let g:easytags_dynamic_files = 1
"let g:easytags_languages = {
"\   'language': {
"\     'cmd': 'jsctags',
"\       'args': [],
"\       'fileoutput_opt': '-f',
"\       'stdout_opt': '-f-',
"\       'recurse_flag': '-R'
"\   }
"\}

" Trigger configuration
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" Keep the lint gutter open always
let g:ale_sign_column_always = 1

nnoremap <leader>q  :wqa
nnoremap <leader>w  :wa<cr>
nnoremap <leader>b  :bd

inoremap <C-c>  <Esc>

set nobackup
set nowb
set noswapfile

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" WhichKey configuration
nnoremap <silent> <leader> :WhichKey ','<CR>
set timeoutlen=500

" set spell spelllang=en_us
