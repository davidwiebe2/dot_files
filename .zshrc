# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export TERM="xterm-256color"

ZSH_THEME="powerlevel10k/powerlevel10k"

plugins=(
    alias-finder
    aws
    bgnotify
    catimg
    colored-man-pages
    colorize
    command-not-found
    cp
    docker
    dotenv
    extract
    git-extras
    gitfast
    gulp
    helm
    kubectl
    last-working-dir
    minikube
    taskwarrior
    tmux
    vi-mode
    wd
    zsh-completions
)

export ZSH=$HOME/.oh-my-zsh
source $ZSH/oh-my-zsh.sh

setopt rm_star_silent
setopt dvorak

export EDITOR='nvim'
alias vim=nvim

alias rg=ripgrep.rg

# Need a way of turning off wrapping via environment variable before using bat as pager
# export PAGER=bat

PATH="$PATH:/usr/local/go/bin"
PATH="$PATH:/snap/bin"
PATH="$PATH:${HOME}/.deno/bin"
PATH="$PATH:${HOME}/.local/bin"
PATH="$PATH:${HOME}/.cargo/bin"

export LESS='--quit-if-one-screen --chop-long-lines --ignore-case --status-column --LONG-PROMPT --RAW-CONTROL-CHARS --HILITE-UNREAD --tabs=4 --no-init --window=-4'

#export PHP_DEBUG_ADDRESS="10.131.25.44"
export ANSIBLE_NOCOWS=1

export NPM_PACKAGES="$HOME/.npmpackages"
export NODE_PATH="$NPM_PACKAGES/lib/node_modules${NODE_PATH:+:$NODE_PATH}"
export PATH="$NPM_PACKAGES/bin:$PATH"
# Unset manpath so we can inherit from /etc/manpath via the `manpath`
# command
unset MANPATH  # delete if you already modified MANPATH elsewhere in your config
export MANPATH="$NPM_PACKAGES/share/man:$(manpath)"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /home/david/.npm-packages/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh ]] && . /home/david/.npm-packages/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /home/david/.npm-packages/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh ]] && . /home/david/.npm-packages/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh
# tabtab source for slss package
# uninstall by removing these lines or running `tabtab uninstall slss`
[[ -f /home/david/.npm-packages/lib/node_modules/serverless/node_modules/tabtab/.completions/slss.zsh ]] && . /home/david/.npm-packages/lib/node_modules/serverless/node_modules/tabtab/.completions/slss.zsh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

_fzf_complete_git() {
    ARGS="$@"
    local branches
    branches=$(git branch -vv --all)
    if [[ $ARGS == 'git co'* || $ARGS == 'git checkout'* || $ARGS == 'git switch'* ]]; then
        _fzf_complete --reverse --multi -- "$@" < <(
            echo $branches
        )
    else
        eval "zle ${fzf_default_completion:-expand-or-complete}"
    fi
}

_fzf_complete_git_post() {
    awk '{print $1}'
}
[[ /usr/local/bin/kubectl ]] && source <(kubectl completion zsh)

# tabtab source for packages
# uninstall by removing these lines
[[ -f ~/.config/tabtab/__tabtab.zsh ]] && . ~/.config/tabtab/__tabtab.zsh || true
