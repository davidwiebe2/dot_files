source-file "${HOME}/dot_files/tmuxline.conf"

set -g escape-time 10
set -g default-terminal "xterm-256color"
set-option -ga terminal-overrides ",xterm-256color:Tc"

# Set prefix to Ctrl-Space
unbind C-b
set -g prefix C-Space
bind Space send-prefix

# Use vi keys
set -gw mode-keys vi
